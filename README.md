# A bash script to record live jupiterbroadcasting video feeds

Requires `rtmpdump` and `at`.

This whole thing needs reworked some day...

Typical cron might look something like:

    35 13 * * Sun /home/username/bin/jupvid.sh LAS >/dev/null 2>&1
