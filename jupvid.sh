#!/bin/bash
today=`date +%Y-%m-%d_%H-%M`
retries=0
cd ~/Downloads
rec () {
    rtmpdump -r rtmp://jblive.videocdn.scaleengine.net/jb-live/play/jblive.stream --live -o $1_$today.flv &
    pget=$!
    if [ $retries == 0 ]; then
        at now + $2 hours <<< "killall rtmpdump"
    fi
    wait $pget
    if [ $? != 2 ]; then
        ((retries++))
        echo "rtmp took a dump or something. Trying again. Attempt: $retries"
        sleep 20
        today=`date +%Y-%m-%d_%H-%M`
        rec $1 $2
    fi
    exit 1
}

if [ $1 == "LAS" ]; then
    rec "LAS" "4"
elif [ $1 == "LUP" ]; then
    rec "LUP" "2"
elif [ $1 == "TTT" ]; then
    rec "TTT" "1"
fi
